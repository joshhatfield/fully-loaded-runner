FROM alpine:3.7

RUN apk update

RUN apk add make vim python3 py-pip jq wget bash

# AWS CLI install

RUN pip install --upgrade pip

RUN pip install awscli --upgrade --user

RUN ln -s /root/.local/bin/aws /usr/local/bin/aws

# Azure Cli

RUN apk add --virtual=build gcc libffi-dev musl-dev openssl-dev python-dev

RUN pip install --user azure-cli

RUN ln -s /root/.local/bin/az /usr/local/bin/az

